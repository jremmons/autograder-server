#!/bin/bash

# move the current working directory to autograder scripts
cd /home/autograder/scripts

# get the wustl key and ensure it's not blank
export WUSTLKEY=$1
if [ -z $WUSTLKEY ]
then
    echo $(date) ": wustl key was empty!" >> log/grade.log
    echo "USAGE: ./grade <WUSTL KEY>"
    exit 1
fi

# source the cilk and grader environment variables
source env.sh

# build and run the tester code AND store output to file
# NOTE: the 'tmp' directory should have 777 permissions or somehow let the Jenkins user write
timeout $MAXRUNTIME"s" ./build_and_test.sh &> tmp/$BUILD_ID.txt

# check to see if the build and run timed-out. If so, add error message to the output so it is logged
# and send to the student for debugging.
if [ $? -eq 124 ]
then
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> tmp/$BUILD_ID.txt
    echo "Job reached maximum runtime ("$MAXRUNTIME "seconds) and was killed!" >> tmp/$BUILD_ID.txt
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> tmp/$BUILD_ID.txt
fi

# cat the output so it will be logged in Jenkins build (can be seen under build->console output)
cat tmp/$BUILD_ID.txt

# get the email address for the student from the student email list
EMAILADDR=$(./get_email_addr.py $WUSTLKEY lists/$STUEMAILLIST)

# send email to the student with the test results attached
cat email_body.txt | mail -s "CSE 341/549 Autograder: $BUILD_ID" -a "From: no-reply@autograde.org" -A tmp/$BUILD_ID.txt -- $EMAILADDR

# clean up the email attachment
rm -f tmp/$BUILD_ID.txt