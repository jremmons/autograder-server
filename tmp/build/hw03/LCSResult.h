// -*- C++ -*-
#ifndef __LCSRESULT_H__
#define __LCSRESULT_H__

/*
 * LCSResult.h
 *
 * Just a convient place to store the result of your LCS algorithm.
 * We have kept everything public to make the coding simpler.
 */

#include <iostream>

using namespace std;

// DO NOT MODIFY THIS FILE !!!!
// (the Auto-Grader will use the original version of this file,
// so any modifications you make here will be discarded before
// your assignment is graded)

class LCSResult {

public:
    // your 2D algorithm needs to fill these 2 fields
    int length;
    string subsequence;

    void print(bool printSubSeq) {
        std::cout << "  Length: " << length << endl;
        if(printSubSeq) {
            std::cout << "  Computed Subsequence: " << subsequence << endl;
        }
    }

    bool equal(LCSResult *other) {
	if(this->length != other->length) {
            return false;
        }
	if(this->subsequence != other->subsequence) {
            return false;
        }
        return true;
    }
};

#endif // __LCSRESULT_H__
