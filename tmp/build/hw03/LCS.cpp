// -*- C++ -*-

/*
 * LCS.cpp
 *
 * HW 03
 *
 */

#include <cstdlib>
#include <cilk/cilk.h>
#include <cilktools/cilkview.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>

#include "LCSResult.h"
#include "LCSSupport.cpp"
#include "Solution.cpp"
#include "StudentSolution.cpp"
#include "Timer.cpp"
using namespace std;

#define MIN(x,y) ((y) ^ (((x) ^ (y)) & -((x) < (y))))
#define MAX_FOR_PRINTING 500

// We'll always print the length of LCS, but only print the tring if
// printLCS_f is set to true.
static bool printLCS_f = true;
static bool printInputStrings_f = true;

static int defaultInputLength = 25;
static unsigned int defaultSeed = 545;

int LCSMain(int length, char* myFile, unsigned int seed) {

    string *seq1 = new string();
    string *seq2 = new string();
    
    std::cout << "-----------------------------------------\n"
      << "LCS: Given two strings, compute the length of longest common "
      << "subsequences in the strings.\n"
      << "-----------------------------------------\n";

    // Initialize stock sequence array
    if (myFile == NULL) {
        seq1->resize(length);
        seq2->resize(length);

  	std::cout << "Randomly generating a string with characters [A-Z] "
		  << "of length " << length << std::endl
                  << "Seed used: " << seed << "." << std::endl;

  	srand(seed); // use the seed provided
        // Create random input strings
        randomlyGenerateString(length, seq1);
        randomlyGenerateString(length, seq2);
    } else {
        // get the sequence from the input file
        getInputStringsFromFile(length, myFile, seq1, seq2);
	length = MIN(seq1->size(), seq2->size());
	seq1->resize(length);
	seq2->resize(length);
    }

    if (printInputStrings_f) {
        if (length <= MAX_FOR_PRINTING) {
            std::cout << "Input string length: " << length << endl; 
            std::cout << "First string:  " << *seq1 << endl; 
            std::cout << "Second string: " << *seq2 << endl; 
        }
    }

    // Compare various LCS algorithms on this sequence.
    Timer *timer2D = new Timer(new string("Serial LCS with 2D DP table"));
    Timer *timer1D = new Timer(new string("Serial LCS with 1D DP table"));
    Timer *timer1DPar = new Timer(
			new string("Recursive Parallel LCS with 1D table"));

    // Recursive Serial 2D
    LCSResult *lcsResult = new LCSResult();
    LCSResult *studentResult = new LCSResult();

    solveLCS2DSol(length, *seq1, *seq2, lcsResult);

    timer2D->printBegin();
    timer2D->begin();
    solveLCS2D(length, *seq1, *seq2, studentResult);
    timer2D->end();
    std::cout << "LCS from 2D serial computation: " << endl;
    studentResult->print(printLCS_f);
    timer2D->print();

    // Recursive Serial 1D
    int table_length = length * 2 + 1;
    int *table1D = new int[table_length];
    memset(table1D, 0, sizeof(table1D[0])*table_length);
    timer1D->printBegin();
    timer1D->begin();
    int lcsLength = solveLCS1D(length, *seq1, *seq2, table1D, 
                               0, length, 0, length);
    timer1D->end();
    std::cout << "LCS length from 1D serial computation: " 
              << lcsLength << endl;
    timer1D->print();

    
    // Recursive Parallel 1D
    memset(table1D, 0, sizeof(table1D[0])*table_length);
    timer1DPar->printBegin();
    timer1DPar->begin();
    // __cilkview_workspan_reset();
    // __cilkview_workspan_start();
    int lcsParLength = solveLCS1DPar(length, *seq1, *seq2, table1D,
				     0, length, 0, length);
    // __cilkview_workspan_stop();
    timer1DPar->end();
    std::cout << "LCS length from 1D parallel computation: " 
              << lcsParLength << endl;
    timer1DPar->print();
    timer1DPar->printSpeedup(timer1D);
    std::cout << "-----------------------------------------\n";
    
    if( lcsResult->equal(studentResult) == false ) {
  	cout << endl;
  	cout << "Serial LCS with 2D DP table INCORRECT" << endl;
  	cout << "Student solution: " << endl;
	studentResult->print(printLCS_f);
  	cout << "Solution: " << endl;
	lcsResult->print(printLCS_f);
  	cout << endl;
    } else {
  	cout << "Serial LCS with 2D DP table correct" << endl;
    }

    if (lcsResult->length != lcsLength) {
 	cout << endl;
  	cout << "Serial LCS with 1D DP table INCORRECT" << endl;
  	cout << "Student solution: " << lcsLength << endl;
  	cout << "Solution:         " << lcsResult->length << endl;
  	cout << endl;
    } else {
  	cout << "Serial LCS with 1D DP table correct" << endl;
    }

    if (lcsResult->length != lcsParLength) {
 	cout << endl;
  	cout << "Parallel LCS with 1D DP table INCORRECT" << endl;
  	cout << "Student solution: " << lcsParLength << endl;
  	cout << "Solution:         " << lcsResult->length << endl;
  	cout << endl;
    } else {
  	cout << "Parallel LCS with 1D DP table correct" << endl;
    }
    std::cout << endl;

    // clean up 
    delete seq1;
    delete seq2;
    delete lcsResult;
    delete studentResult;
    delete table1D;

    delete timer2D;
    delete timer1D;
    delete timer1DPar;

    return 0;
}

static void printUsage() {
    std::cout << "--------- LCS -help -------------------" << std::endl;
    std::cout << "Usage: ./LCS -f <filename> [-pLCS <0|1>] [-pInput <0|1>]\n" 
        << "        --OR--" << std::endl 
        << "       ./LCS -len <length> [-pLCS <0|1>] [-pInput <0|1>]\n"
        << "       where: " << std::endl 
        << " -f filename specifies the input file to compute LCS on,\n" 
        << "    where first line of the file contains the first input string\n" 
        << "    and second line of the file contains the second input string.\n"
        << std::endl
        << " -pLCS 1|0    whether you want the result LCS to be printed"
        << " (Default = " << printLCS_f << ")" << std::endl
        << " -pInput 1|0  whether you want the input strings to be printed"
        << " (Default = " << printInputStrings_f << ")" << std::endl 
	<< std::endl
        << "The following arguments apply ONLY to randomly generated strings:" 
        << std::endl
        << "    (which will occur if no input file is specified)" << std::endl
        << " -len x limits the input string length to x (Default = " 
        << defaultInputLength << ")" << std::endl
        << " -s s gives the seed for randomly generating the strings"
        << " (Default = " << defaultSeed << ")" << std::endl;
}

int main(int argc, char* argv[]) {

    char *myFile = NULL;
    unsigned int seed = defaultSeed;
    int len = defaultInputLength;

    if (argc > 1) {
        // they might just be asking for usage information
        if (std::string(argv[1]) == "-help") {
            printUsage();
	    exit(0);
        }
    }

    // or they might actually want to run the code!
    for (int i = 1; i < argc; i=i+2) {
        if (std::string(argv[i]) == "-f") {
            // We know the next argument *should* be the filename:
            myFile = argv[i + 1];
        } else if (std::string(argv[i]) == "-len") {
            // We know the next argument *should* be first input length:
            len = atoi(argv[i + 1]);
        } else if (std::string(argv[i]) == "-pLCS") {
            // We know the next argument *should* be 1 or 0
            printLCS_f = (atoi(argv[i + 1]) == 0) ? false : true;
        } else if (std::string(argv[i]) == "-pInput") {
            // We know the next argument *should* be 1 or 0
            printInputStrings_f = (atoi(argv[i + 1]) == 0) ? false : true;
        } else if (std::string(argv[i]) == "-s") {
            // We know the next argument *should* be the number for the seed 
            int tmp = atoi(argv[i + 1]);
            if(tmp < 0) seed = -tmp; else seed = tmp;
        }
    }

    std::cout << "For a complete explanation of command line arguments,"
              << " type: ./LCS -help" << std::endl;

    return LCSMain(len, myFile, seed);
}
