// -*- C++ -*-

/*
 * LCSSupport.cpp
 *
 * Protecting 341 students from the ugliness that is files, random
 * number generation, list creation, and print methods.
 *
 */

#include <cstdlib>
#include <iostream>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

// DO NOT MODIFY THIS FILE !!!!
// (the Auto-Grader will use the original version of this file,
// so any modifications you make here will be discarded before
// your assignment is graded)

static void checkLen(int len) {
  if (len < 2) {
    cout << "Sequence Length must be > 1";
    exit(0);
  }
}

void getInputStringsFromFile(int length, char* filename, 
                             string *seq1, string *seq2) {

  int file_len;

  ifstream myfile(filename);

  if (myfile.is_open()) {
      int i = 0;
      if( myfile.good() ) {
          getline(myfile, *seq1);
      }
      if( myfile.good() ) {
          getline(myfile, *seq2);
      }
      myfile.close();

  } else {
    cout << "Unable to open file " << filename << std::endl; 
    exit(0);
  }
}

// This code already works and you shouldn't have to understand or
// modify it, so we've stashed it here.
void randomlyGenerateString(int length, string *str) {
  
  checkLen(length);

  str->resize(length);
  for(int i = 0; i < length; i++) {
    // Populate seq 
    (*str)[i] = (char) ((rand() % 26) + 65);
  }
}

