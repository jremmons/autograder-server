// -*- C++ -*-

/*
 * StudentSolution.cpp
 *
 * HW 03
 *
 * THIS is the only file we will look at when we grade your homework.
 * All other changes you made to any other files will be discarded.
 */

#include <cstdlib>
#include <cilk/cilk.h>

#include "LCSResult.h"

using namespace std;


void printLCS(int **b, const string &s1, int i, int j, LCSResult *res){
	if (i==0 || j ==0) return;
	if (b[i][j] == 2){
		printLCS(b, s1, i-1, j-1, res);
		(res->subsequence).append(string(1,s1.at(i-1)));
	}
	else if (b[i][j] == 3)
		printLCS(b,s1,i-1,j, res);
	else
		printLCS(b,s1,i,j-1, res);
}
/* 
 * This method implements the serial algorithm of computing the longest
 * common subsequence of two strings.  This algorithm should also include 
 * the backtracking code to produce the actual longest common subsequence 
 * (to be included in LCSResult).  The result is to be returned via 
 * LCSResult (defined in LCSResult.h).  You can implement your algorithm 
 * using either divide and conquer or a doubly nested for-loops to fill the
 * dynamic programming table. 
 */
void solveLCS2D(int length, const string &s1, const string &s2, 
                LCSResult *res) {
	int **b;
	b = new int *[s1.length()+1];
	for (int i = 0; i <= s1.length(); i++)
		b[i] = new int[s2.length()+1];
	int **c;
	c = new int *[s1.length()+1];
	for (int i = 0; i <= s1.length(); i++)
		c[i] = new int[s2.length()+1];
	int i, j;
	for (i = 1; i <= s1.length(); i++)
		c[i][0] = 0;
	for (i = 0; i <= s2.length(); i++)
		c[0][i] = 0;
	for (i = 1; i <= s1.length(); i++){
		for (j = 1; j <= s2.length(); j++){
			if (s1.at(i-1) == s2.at(j-1)){
				c[i][j] = c[i-1][j-1] + 1;
				b[i][j] = 2; // go left and then up
			}
			else if (c[i-1][j] >= c[i][j-1]){
				c[i][j] = c[i-1][j];
				b[i][j] = 3; // go up
			}
			else {
				c[i][j] = c[i][j-1];
				b[i][j] = 1; // go left
			}
		}
	}
	res->length = c[s1.length()][s2.length()];
	printLCS(b,s1,s1.length(),s2.length(),res);
}


/* 
 * This method implements the serial divide and conquer algorithm of 
 * computing the longest common subsequence of two strings, using only
 * O(n) space.  This algorithm does not need to compute the actual longest 
 * common subsequence; it needs to only compute and return the length of 
 * the longest common subsequence.  Your algorithm should use only the 
 * table provided to you as an input pararameter and should not dynamically 
 * allocate any additional space.  
 */
int solveLCS1D(int length, const string &s1, const string &s2,
               int *table, int i1, int i2, int j1, int j2) {
	//base cases
	int n = s1.length();
	if (length <= 2){ //three subcases
		int i, j;
		for(i=i1; i<i2; i++){
			for(j=j1; j<j2; j++){
				if (s1.at(i) == s2.at(j)){
					table[i-j+n] += 1;
				}
				else if(table[i-j+n+1] >= table[i-j+n-1]){
					table[i-j+n] = table[i-j+n+1];
				}
				else{
					table[i-j+n] = table[i-j+n-1];
				}
			}
		}
	}	
	else{//divide
		int l = length/2;
		solveLCS1D(l, s1, s2, table,i1,i1+(i2-i1)/2,j1,j1+(j2-j1)/2); 

		solveLCS1D(l, s1, s2, table,i1+(i2-i1)/2,i2,j1,j1+(j2-j1)/2); 

		solveLCS1D(l, s1, s2, table,i1,i1+(i2-i1)/2,j1+(j2-j1)/2,j2); 

		solveLCS1D(l, s1, s2, table,i1+(i2-i1)/2,i2,j1+(j2-j1)/2,j2); 
		// no need to merge
	}
    return table[n];
}


/* 
 * This method implements the parallel version of the divide and conquer
 * algorithm of computing the longest common subsequence of two strings, 
 * using only O(n) space.   
 */
int solveLCS1DPar(int length, const string &s1, const string &s2,
               	  int *table, int i1, int i2, int j1, int j2) {
	//base cases
	int n = s1.length();
	if (length <= 2){ //three subcases
		int i, j;
		for(i=i1; i<i2; i++){
			for(j=j1; j<j2; j++){
				if (s1.at(i) == s2.at(j)){
					table[i-j+n] += 1;
				}
				else if(table[i-j+n+1] >= table[i-j+n-1]){
					table[i-j+n] = table[i-j+n+1];
				}
				else{
					table[i-j+n] = table[i-j+n-1];
				}
			}
		}
	}	
	else{//divide
		int l = length/2;
		solveLCS1DPar(l, s1, s2, table,i1,i1+(i2-i1)/2,j1,j1+(j2-j1)/2); 

		cilk_spawn solveLCS1DPar(l, s1, s2, table,i1+(i2-i1)/2,i2,j1,j1+(j2-j1)/2); 

		cilk_spawn solveLCS1DPar(l, s1, s2, table,i1,i1+(i2-i1)/2,j1+(j2-j1)/2,j2); 

		cilk_sync;

		solveLCS1DPar(l, s1, s2, table,i1+(i2-i1)/2,i2,j1+(j2-j1)/2,j2); 
		// no need to merge
	}
    return table[n];
}
