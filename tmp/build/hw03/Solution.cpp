// -*- C++ -*-

/*
 * Solution.cpp
 *
 * HW 03 Solution
 */

#include <cstdlib>
// #include <cilk/cilk.h>

#include "LCSResult.h"

using namespace std;

// just for debugging
#ifndef DEBUG
#define DEBUG 1
#endif

#if DEBUG
// 0 after die to get rid of compiler error that 'die' does not return an expression
#define assert_with_info(ex) ((ex) || (die(#ex, __LINE__), 0))
#define WHEN_DEBUG(ex) ex
#else
#define assert_with_info(ex)
#define WHEN_DEBUG(ex)
#endif



typedef enum {ABOVE, LEFT, LEFT_DIAG, NONE} table_val;

static void die(string failed_ex, int line) {
    cerr << "FAILED: Assertion " << failed_ex 
	 << " failed at line " << line << endl;
    
    exit(-1);
}

static void print_table(int *table, int length) {
    
#define table(i,j) table[((i)*(length+1) + (j))]
    for(int i=0; i <= length; i++) {
        for(int j=0; j <= length; j++) {
            cout << table(i,j) << ", ";
        }
        cout << endl;
    }
#undef table
}

static void print_bt_table(table_val *bt_table, int length) {
    
#define bt_table(i,j) bt_table[((i)*(length+1) + (j))]
    for(int i=0; i <= length; i++) {
        for(int j=0; j <= length; j++) {
            if(bt_table(i,j) == LEFT_DIAG) {
                cout << "\\ ";
            } else if(bt_table(i,j) == LEFT) {
                cout << "- ";
            } else if(bt_table(i,j) == ABOVE) {
                cout << "| ";
            } else {
                cout << "  ";
            }
        }
        cout << endl;
    }
#undef bt_table
}

static void 
backtrackLCS2D_Helper(int length, const string &s1, const string &s2,
                      table_val *bt_table, LCSResult *res) {
#define bt_table(i,j) bt_table[((i)*(length+1) + (j))]
    int res_index = res->length - 1;
    string res_str;
    res_str.resize(res->length);

    int i=length, j=length;
    while (i > 0 && j > 0) {
        if (bt_table(i,j) == LEFT_DIAG) {
            assert_with_info(res_index >= 0);
            res_str[res_index] = s1[i-1]; // string index is off by 1
            res_index--;
            i--;
            j--;
        } else if (bt_table(i,j) == LEFT) {
            j--;  
        } else if (bt_table(i,j) == ABOVE) {
            i--;  
        }
    }

    res->subsequence = res_str;
#undef bt_table
}

static void solveLCS_simple(int length, const string &s1, 
                            const string &s2, LCSResult *res) {

    int table_size = (length+1) * (length+1);
    int *table = new int[table_size]; 
    table_val *bt_table = new table_val[table_size]; 

// use Macro to index the tables
#define table(i,j) table[((i)*(length+1) + (j))]
#define bt_table(i,j) bt_table[((i)*(length+1) + (j))]
    for(int i=0; i < length+1; i++) {
	table(0,i) = 0;
	table(i,0) = 0;
	bt_table(0,i) = ABOVE;
	bt_table(i,0) = LEFT;
    }
    
    for (int i = 1; i < length+1; i++) {
        for (int j = 1; j < length+1; j++) {  
            // per row, traverse through each col
            // update len[i][j] based on the recursive formula 
            if (s1[i-1] == s2[j-1]) {  // case 2
                table(i,j) = table(i-1,j-1) + 1;
                bt_table(i,j) = LEFT_DIAG;
            } else if (table(i-1,j) >= table(i,j-1)) {
                table(i,j) = table(i-1,j);
                bt_table(i,j) = ABOVE; 
            } else {
                table(i,j) = table(i,j-1);
                bt_table(i,j) = LEFT;
            }
        }
    }
    res->length = table(length,length);
    backtrackLCS2D_Helper(length, s1, s2, bt_table, res);

    delete[] table;
    delete[] bt_table;
#undef table
#undef bt_table
}

// l1: str1 length, l2: str2 length
// table: the table keeping track of LCS
// bt_table: the backtracking table keeping track of where we got our LCS
// this instance is filling the tables between row i1-i2 and col j1-j2
static void solveLCS2D_DCHelper(int length, const string &s1, const string &s2, 
			        int *table, table_val *bt_table,
		   	        int i1, int i2, int j1, int j2) {

// use Macro to index the tables
#define table(i,j) table[((i)*(length+1) + (j))]
#define bt_table(i,j) bt_table[((i)*(length+1) + (j))]

    // base case 
    if ((i2-i1) == 1 && (j2-j1) == 1) {
 	if (s1[i1-1] == s2[j1-1]) {
	    table(i1, j1) = table(i1-1, j1-1) + 1;
            bt_table(i1, j1) = LEFT_DIAG;
	} else if (table(i1-1, j1) >= table(i1, j1-1)) { 
	    table(i1, j1) = table(i1-1, j1);
            bt_table(i1, j1) = ABOVE;
        } else {
            table(i1, j1) = table(i1, j1-1);
            bt_table(i1, j1) = LEFT;
	}
    } else {
	// since the table size may not start as divisible by 2, it may 
	// be necessary to recur down on one of the dimension more
        int mid_i = (i2-i1) > 1 ? (i1 + (i2-i1) / 2) : i2;
        int mid_j = (j2-j1) > 1 ? (j1 + (j2-j1) / 2) : j2;
        assert_with_info(mid_i < i2 || mid_j < j2);
        // quadrant 0,0
        solveLCS2D_DCHelper(length, s1, s2, table, bt_table, 
		            i1, mid_i, j1, mid_j); 
        // quadrant 0,1 
	if (mid_j < j2) {
            solveLCS2D_DCHelper(length, s1, s2, table, bt_table, 
			        i1, mid_i, mid_j, j2); 
	}
        // quadrant 1,0 
	if (mid_i < i2) {
            solveLCS2D_DCHelper(length, s1, s2, table, bt_table, 
			        mid_i, i2, j1, mid_j); 
        }
        // quadrant 1,1 
	if (mid_i < i2 && mid_j < j2) {
            solveLCS2D_DCHelper(length, s1, s2, table, bt_table,  
	        		mid_i, i2, mid_j, j2); 
        }
    }
#undef table
#undef bt_table
}

// YOUR LCS serial algorithm using a 2D table
// This algorithm should also include the backtracking code to produce the 
// actual longest common subsequence (to be included in LCSResult).
void solveLCS2DSol(int length, const string &str1, const string &str2, 
                   LCSResult *res) {

// use Macro to index the tables
#define table(i,j) table[((i)*(length+1) + (j))]
#define bt_table(i,j) bt_table[((i)*(length+1) + (j))]
    int table_size = (length+1) * (length+1);
    int *table = new int[table_size]; 
    table_val *bt_table = new table_val[table_size]; 

    for(int i=0; i < length+1; i++) {
	table(0,i) = 0;
	table(i,0) = 0;
	bt_table(0,i) = ABOVE;
	bt_table(i,0) = LEFT;
    }
    
    // row 0 and col 0 filled already; starts with row 1 col 1
    solveLCS2D_DCHelper(length, str1, str2, 
			table, bt_table, 1, length+1, 1, length+1);  
    res->length = table(length, length);
    backtrackLCS2D_Helper(length, str1, str2, bt_table, res);

    delete[] table;
    delete[] bt_table;
#undef table
    
    LCSResult tmp;
    solveLCS_simple(length, str1, str2, &tmp);
    assert_with_info(res->length == tmp.length);
    assert_with_info(res->subsequence == tmp.subsequence);
}


// YOUR recursive, divide and conqure LCS serial algorithm using a 1D table
// This algorithm computes only the length of the longest common subsequence, 
// which it then returns.
int solveLCS1DSol(int length, const string &s1, const string &s2,
                  int *table, int i1, int i2, int j1, int j2) {
// use Macro to index the tables
// properly, it's table-size (length+1) - i + j - 1 (-1 for indexing)
// doesn't actually matter whether the index is for string or the table; 
// the table indexing is the same.  The only diff is the string indexing.
#define table(i,j) table[length-(i)+(j)]
#define table_index(i,j) (length-(i)+(j))
    assert_with_info(table_index(i1,j1) >= 0 &&
		     table_index(i1,j1) < (length*2 + 1));  
    assert_with_info(table_index(i1-1,j1-1) >= 0 &&
		     table_index(i1-1,j1-1) < (length*2 + 1));  
    assert_with_info(table_index(i1-1,j1) >= 0 &&
		     table_index(i1-1,j1) < (length*2 + 1));  
    assert_with_info(table_index(i1,j1-1) >= 0 &&
		     table_index(i1,j1-1) < (length*2 + 1));  
    // base case 
    if ((i2-i1) == 1 && (j2-j1) == 1) {
 	if (s1[i1] == s2[j1]) {
	    table(i1, j1) = table(i1-1, j1-1) + 1;
	} else if (table(i1-1, j1) >= table(i1, j1-1)) { 
	    table(i1, j1) = table(i1-1, j1);
        } else {
            table(i1, j1) = table(i1, j1-1);
	}
    } else {
	// since the table size may not start as divisible by 2, it may 
	// be necessary to recur down on one of the dimension more
        int mid_i = (i2-i1) > 1 ? (i1 + (i2-i1) / 2) : i2;
        int mid_j = (j2-j1) > 1 ? (j1 + (j2-j1) / 2) : j2;
        assert_with_info(mid_i < i2 || mid_j < j2);
        // quadrant 0,0
        solveLCS1DSol(length, s1, s2, table, i1, mid_i, j1, mid_j); 
        // quadrant 0,1 
	if (mid_j < j2) {
            solveLCS1DSol(length, s1, s2, table, i1, mid_i, mid_j, j2); 
	}
        // quadrant 1,0 
	if (mid_i < i2) {
            solveLCS1DSol(length, s1, s2, table, mid_i, i2, j1, mid_j); 
        }
        // quadrant 1,1 
	if (mid_i < i2 && mid_j < j2) {
            solveLCS1DSol(length, s1, s2, table, mid_i, i2, mid_j, j2); 
        }
    }

    return table(length, length);
#undef table
#undef table_index
}

// YOUR recursive, divide and conqure LCS *parallel* algorithm using a 1D table
// This algorithm computes only the length of the longest common subsequence, 
// which it then returns.
int solveLCS1DParSol(int length, const string &s1, const string &s2,
               	     int *table, int i1, int i2, int j1, int j2) {

#define table(i,j) table[length-(i)+(j)]
#define table_index(i,j) (length-(i)+(j))
    assert_with_info(table_index(i1,j1) >= 0 &&
		     table_index(i1,j1) < (length*2 + 1));  
    assert_with_info(table_index(i1-1,j1-1) >= 0 &&
		     table_index(i1-1,j1-1) < (length*2 + 1));  
    assert_with_info(table_index(i1-1,j1) >= 0 &&
		     table_index(i1-1,j1) < (length*2 + 1));  
    assert_with_info(table_index(i1,j1-1) >= 0 &&
		     table_index(i1,j1-1) < (length*2 + 1));  
    // base case 
    if ((i2-i1) == 1 && (j2-j1) == 1) {
 	if (s1[i1] == s2[j1]) {
	    table(i1, j1) = table(i1-1, j1-1) + 1;
	} else if (table(i1-1, j1) >= table(i1, j1-1)) { 
	    table(i1, j1) = table(i1-1, j1);
        } else {
            table(i1, j1) = table(i1, j1-1);
	}
    } else {
	// since the table size may not start as divisible by 2, it may 
	// be necessary to recur down on one of the dimension more
        int mid_i = (i2-i1) > 1 ? (i1 + (i2-i1) / 2) : i2;
        int mid_j = (j2-j1) > 1 ? (j1 + (j2-j1) / 2) : j2;
        assert_with_info(mid_i < i2 || mid_j < j2);
        // quadrant 0,0
        solveLCS1DParSol(length, s1, s2, table, i1, mid_i, j1, mid_j); 
        // quadrant 0,1 
	if (mid_j < j2) {
            solveLCS1DParSol(length, s1, s2, table, i1, mid_i, mid_j, j2); 
	}
        // quadrant 1,0 
	if (mid_i < i2) {
            solveLCS1DParSol(length, s1, s2, table, mid_i, i2, j1, mid_j); 
        }
        // quadrant 1,1 
	if (mid_i < i2 && mid_j < j2) {
            solveLCS1DParSol(length, s1, s2, table, mid_i, i2, mid_j, j2); 
        }
    }

    return table(length, length);
#undef table
#undef table_index
}
