// -*- C++ -*-

/*
 * ApspSupport.cpp
 *
 * Protecting 341 students from the ugliness that is files, random
 * number generation, list creation, and print methods.
 *
 */

#include <cstdlib>
#include <iostream>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <limits>
#include <math.h>
using namespace std;

#define MAX_WEIGHT 10 
typedef int weight_t;

// DO NOT MODIFY THIS FILE !!!!
// (the Auto-Grader will use the original version of this file,
// so any modifications you make here will be discarded before
// your assignment is graded)

void getMatrixFromFile(vector<vector<weight_t> > &matrix, char* filename) {
  int file_len;
  string line;

  ifstream myfile (filename);
  if (myfile.is_open()) {
      int i = 0;
      bool first = true;
      while (myfile.good() && getline(myfile, line)) {
          if (first) {
            int newSize = atoi(line.c_str());
            first = false;
            matrix.resize(newSize);
            for (int k = 0; k < matrix.size(); k++) {
              matrix[k].resize(newSize);
            }
            continue;
          }
          if (i >= matrix.size()) {
            cout << "Too many rows of elements!" << endl;
            exit(0);
          }
          istringstream iss(line);
          string number;
          int j = 0;
          while(getline(iss, number, ' ')) {
            if (j >= matrix[0].size()) {
              cout << "Too many columns of elements!" << endl;
              exit(0);
            }
            if (number.compare("inf") == 0) {
              matrix[i][j] = numeric_limits<weight_t>::max();
            } else {
              matrix[i][j] = atof(number.c_str());
            }
            j++;
          }
          i++;
      }
      myfile.close();
  }
  else {
      cout << "Unable to open file " << filename << endl; 
      exit(0);
  }
}

// This code already works and you shouldn't have to understand or
// modify it, so we've stashed it here.
void randomlyGenerateMatrix(vector< vector<weight_t> > &matrix,
                            double sparsity) {
  weight_t maxValue = (numeric_limits<weight_t>::max() / matrix.size()) - 1;
  if (maxValue > MAX_WEIGHT) { maxValue = MAX_WEIGHT; }

  int size = matrix.size();
  printf("Randomly generating matrix of dimensions: %dx%d\n", size, size);
  printf("Highest possible edge weight = %d\n", maxValue);
  printf("Using sparsity %.4f\n", sparsity);

  for (int i = 0; i < matrix.size(); i++) {
    for (int j = 0; j < matrix[0].size(); j++) {
      // Populate matrix
      if (i == j) {
        matrix[i][j] == 0;
        continue;
      }
      double exists = (double)rand() / (double)RAND_MAX;
      if (exists < sparsity) {
        double rand_val = (double)rand()/(double)RAND_MAX * maxValue;
        matrix[i][j] = floor(maxValue - rand_val) + 1; // prevent 0-weight edge
      }
      else {
        matrix[i][j] = numeric_limits<weight_t>::max();
      }
    }
  }

  vector<int> connected;
  connected.push_back(matrix.size() - 1);
  vector<int> unconnected;
  for (int i = 0; i < matrix.size() - 1; i++) {
    unconnected.push_back(i);
  }

  random_shuffle(unconnected.begin(), unconnected.end());

  for (vector<int>::iterator itr = unconnected.begin(); 
       itr != unconnected.end(); ++itr) {
    int connectTo = rand() % connected.size();
    int connectFrom = rand() % connected.size();
    double rand_val = 0.0f;
    rand_val = (double)rand()/(double)RAND_MAX * maxValue;
    matrix[connected[connectTo]][*itr] = floor(maxValue- rand_val) + 1;
    rand_val = (double)rand()/(double)RAND_MAX * maxValue;
    matrix[*itr][connected[connectFrom]] = floor(maxValue - rand_val) + 1; 
    connected.push_back(*itr);
  }
}

void printMatrix(vector<vector<weight_t> > &matrix) {
  cout << "\t";
  for (int i = 0; i < matrix.size(); i++) {
    printf("%d\t", i);
  }
  cout << endl << endl;
  for (int i = 0; i < matrix.size(); i++) {
    printf("%d\t", i);
    for (int j = 0; j < matrix[0].size(); j++) {
      if (matrix[i][j] == numeric_limits<weight_t>::max()) {
        cout << "inf" << "\t";
      }
      else {
        // printf("%.2f\t", matrix[i][j]);
        printf("%d\t", matrix[i][j]);
      }
    }
    cout << endl;
  }
  cout << endl;
}

