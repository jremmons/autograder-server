// -*- C++ -*-

/*
 * StudentSolution.cpp
 *
 * HW 04
 *
 * THIS is the only file we will look at when we grade your homework.
 * All other changes you made to any other files will be discarded.
 */

#include <algorithm>
#include <cilk/cilk.h>
#include <cilk/reducer_min.h>
#include <cstdlib>
#include <iostream>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>

using namespace std;

typedef int weight_t;

weight_t inf = numeric_limits<weight_t>::max();

// Your matrix multiplication
// Takes in two matrixes and performs the matrix multiplication using the 
// operations that the apsp algorithm uses.  
// Do not moidfy this signature.  Used by the autograder.
void matrixMultSequential(vector<vector<weight_t> > &matrixA,
                          vector<vector<weight_t> > &matrixB, 
                          vector<vector<weight_t> > &result)
{
  size_t outerDim = matrixA.size();
  size_t innerDim = matrixB.size();

  for (int i = 0; i < outerDim; i++) {

    for (int j = 0; j < outerDim; j++) {
      result[i][j] = inf;

      for (int k = 0; k < innerDim; k++) {
        weight_t x = matrixA[i][k];
        weight_t y = matrixB[k][j];
        weight_t path = (x == inf || y == inf) ? inf : x + y;
        result[i][j] = std::min(result[i][j], path);
      }
    }
  }

}

// Your parallel matrix multiplication
// Takes in two matrixes and performs the matrix multiplication using the 
// operations that the apsp algorithm uses.  
// Do not moidfy this signature.  Used by the autograder.
void matrixMultParallel(vector<vector<weight_t> > &matrixA,
                        vector<vector<weight_t> > &matrixB,
                        vector<vector<weight_t> > &result)
{
  size_t size = matrixA.size();

  cilk_for (int i = 0; i < size; i++) {
    cilk_for (int j = 0; j < size; j++) {
      result[i][j] = inf;
      cilk::reducer< cilk::op_min<weight_t> > best;

      cilk_for (int k = 0; k < size; k++) {
        weight_t x = matrixA[i][k];
        weight_t y = matrixB[k][j];
        weight_t path = (x == inf || y == inf) ? inf : x + y;
        best->calc_min(path);
      }
      result[i][j] = best.get_value();
    }
  }
}

bool negCycleExists(vector<vector<weight_t> > &distances)
{
  bool neg_cycle = false;
  size_t size = distances.size();
  cilk_for (int i = 0; i < size; i++) {
    // race condition doesn't matter; only set to one value
    if (distances[i][i] < 0) neg_cycle = true;
  }
  return neg_cycle;

}

// Your parallel APSP algorithm using matrix multiplication.  
// You should use use / call the matrixMultParallel subroutine you wrote.   
// Return value of false indicates a negative weight cycle.
// Do not modify this signature.  Used by the autograder.
bool apspMMParallel(vector<vector<weight_t> > &weights, 
                    vector<vector<weight_t> > &distances)
{
  size_t size = weights.size();
  int num_edges;

  for (num_edges = 1; num_edges <= size; num_edges += num_edges) {
    matrixMultParallel(weights, weights, distances);
    weights.swap(distances);
  }
  if (num_edges < size) matrixMultParallel(weights,weights,distances);

  weights.swap(distances);

  return !negCycleExists(distances);
}

// Your parallel APSP algorithm using Floyd Warshall Algorithm.
// Return value of false indicates a negative weight cycle. 
// Do not modify this signature.  Used by the autograder.
bool floydWarshallParallel(vector<vector<weight_t> > &weights,
                           vector<vector<weight_t> > &distances)
{
  size_t size = weights.size();

  for (int k = 0; k < size; k++) {
    cilk_for (int i = 0; i < size; i++) {
      cilk_for (int j = 0; j < size; j++) {
        weight_t x = weights[i][k];
        weight_t y = weights[k][j];
        weight_t path = (x == inf || y == inf) ? inf : x + y;
        distances[i][j] = std::min(weights[i][j], path);
      }
    }
    weights.swap(distances);
  }
  weights.swap(distances);

  return !negCycleExists(distances);
}
