// -*- C++ -*-

/*
 * Apsp.cpp
 *
 * HW 04
 *
 */

#include <cstdlib>
#include <cilk/cilk.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <limits>
#include <time.h>
#include <math.h>
#include "ApspSupport.cpp"
#include "StudentSolution.cpp"
#include "Solution.cpp"
#include "Timer.cpp"

#include <cilktools/cilkview.h>
using namespace std;

// DO NOT MODIFY THIS FILE !!!!
// (the Auto-Grader will use the original version of this file,
// so any modifications you make here will be discarded before
// your assignment is graded)

static int defaultMatrixSize  = 10;
static bool printResult_f = false;
static bool printMatrix_f = false;
static bool testMM = true;

static double sparsity = 0.50; // How sparse our grah is
static double seed = 337; // seed used for randomly generated matrix 

int apspMain(int matrixSize, char* myFile) {

  vector<vector<weight_t> > weights(matrixSize, vector<weight_t>(matrixSize));

  // Initialize graph
  if (myFile == NULL) {
    // Create random graph
    randomlyGenerateMatrix(weights, sparsity);
  } else {
    // get the graph from the input file
    getMatrixFromFile(weights, myFile);
    matrixSize = weights.size();
  }
  // don't print a really big graph. that's madness
  printMatrix_f = printMatrix_f;
  if (printMatrix_f) {
    printf("Weights :\n");
    printMatrix(weights);
  }

  // Create graphs for the sequential and parallel algorithms
  vector< vector<weight_t> > copyWeights(matrixSize, 
                                         vector<weight_t>(matrixSize));
  vector< vector<weight_t> > solWeights(matrixSize, 
                                        vector<weight_t>(matrixSize));
  vector< vector<weight_t> > distancesSol(matrixSize, 
                                          vector<weight_t>(matrixSize));
  vector< vector<weight_t> > distancesApspMM(matrixSize, 
                                             vector<weight_t>(matrixSize));
  vector< vector<weight_t> > distancesFW(matrixSize, 
                                         vector<weight_t>(matrixSize));
  for (int i = 0; i < weights.size(); i++) {
    for (int j = 0; j < weights[0].size(); j++) {
      solWeights[i][j] = copyWeights[i][j] = weights[i][j];
      distancesSol[i][j] = distancesApspMM[i][j] = distancesFW[i][j] 
                         = numeric_limits<weight_t>::max();
    }
  }

  // Compare various APSP algorithms on this sequence.
  Timer *apspTimer = new Timer(new string("APSP using MM"));
  Timer *FWTimer = new Timer(new string("APSP using Floyd Warshall"));

  // Test the matrix multiplication algorithm as well
  if (testMM) {
    Timer *sequentialMM = new Timer(new string("Sequential Matrix Multiply"));
    Timer *parallelMM = new Timer(new string("Parallel Matrix Multiply"));

    vector< vector<weight_t> > mmASeq(matrixSize, vector<weight_t>(matrixSize));
    vector< vector<weight_t> > mmAPar(matrixSize, vector<weight_t>(matrixSize));
    vector< vector<weight_t> > mmSol(matrixSize, vector<weight_t>(matrixSize));
    vector< vector<weight_t> > mmResultSeq(matrixSize, 
                                           vector<weight_t>(matrixSize));
    vector< vector<weight_t> > mmResultPar(matrixSize, 
                                           vector<weight_t>(matrixSize));
    vector< vector<weight_t> > mmResultSol(matrixSize, 
                                           vector<weight_t>(matrixSize));

    for (int i = 0; i < matrixSize; i++) {
      for (int j = 0; j < matrixSize; j++) {
        mmSol[i][j] = mmASeq[i][j] = mmAPar[i][j] = weights[i][j];
        mmResultSol[i][j] = mmResultSeq[i][j] = mmResultPar[i][j] 
                          = numeric_limits<weight_t>::max();
      }
    }

    matrixMultSequentialSolution(mmSol, mmSol, mmResultSol);

    sequentialMM->begin();
    matrixMultSequential(mmASeq, mmASeq, mmResultSeq);
    sequentialMM->end();
    parallelMM->begin();
    matrixMultParallel(mmAPar, mmAPar, mmResultPar);
    parallelMM->end();

    bool sequentialCorrect = true;
    bool parallelCorrect = true;
    for (int i = 0; i < matrixSize; i++) {
      for (int j = 0; j < matrixSize; j++) {
        if (mmResultSol[i][j] != mmResultSeq[i][j]) {
          sequentialCorrect = false;
        }
        if (mmResultSol[i][j] != mmResultPar[i][j]) {
          parallelCorrect = false;
        }
      }
    }

    if (sequentialCorrect) {
      cout << "seqmm correct" << endl;
    } else {
      cout << "SEQUENTIAL MATRIX MULTIPL INCORRECT" << endl;
    }

    if (parallelCorrect) {
      cout << "parmm correct" << endl;
    }  else {
      cout << "PARALLEL MATRIX MULTIPL INCORRECT" << endl;
    }
    cout << endl;
  }

  bool solRet = apspSequentialSolution(solWeights, distancesSol);

  // Run the parallel apspMM and time it!
  apspTimer->printBegin();
  apspTimer->begin();
  bool apspRet = apspMMParallel(weights, distancesApspMM);
  apspTimer->end();
  apspTimer->print();

  // Run the parallel Floyd Warshall and time it!
  FWTimer->printBegin();
  FWTimer->begin();
  bool FWRet = floydWarshallParallel(copyWeights, distancesFW);
  FWTimer->end();
  FWTimer->print();

  bool apspCorrect = true;
  bool FWCorrect = true;
  int diff = 0; // print upto 10 distances that differ

  cout << "\nChecking answers for computing APSP using MM." << endl;
  if (solRet != apspRet) {
      cout << "Sol return: " << solRet 
           << ", stud return: " << apspRet << endl;
      apspCorrect = false;
  } else {
    for (int i = 0; i < matrixSize; i++) {
      for (int j = 0; j < matrixSize; j++) {
        if (distancesSol[i][j] != distancesApspMM[i][j]) {
          if (diff < 10) {
            if (distancesSol[i][j] == numeric_limits<weight_t>::max()) {
              //cout << "Sol [" << i << "][" << j << "] = inf";
            } else {
              //cout << "Sol [" << i << "][" << j << "] = " << distancesSol[i][j];
            }
            if (distancesApspMM[i][j] == numeric_limits<weight_t>::max()) {
              //cout << ", Stud[" << i << "][" << j << "] = inf";
            } else {
              //cout << ", Stud[" << i << "][" << j << "] = " << distancesApspMM[i][j];
            }
              //cout << endl;
            diff++;
          }
        }
      }
    }
  }
  if(diff) { apspCorrect = false; }
  if (apspCorrect) {
    cout << "APSP using MM correct" << endl << endl;
  } else {
    cout << "APSP using MM INCORRECT" << endl << endl;
  }

  diff = 0;
  cout << "Checking answers for computing APSP using Floyd Warshall." << endl;
  if (solRet != FWRet) {
      cout << "Sol return: " << solRet 
           << ", stud return: " << FWRet << endl;
      FWCorrect = false;
  } else {
    for (int i = 0; i < matrixSize; i++) {
      for (int j = 0; j < matrixSize; j++) {
        if (distancesSol[i][j] != distancesFW[i][j]) {
          if (diff < 10) {
            if (distancesSol[i][j] == numeric_limits<weight_t>::max()) {
              //cout << "Sol [" << i << "][" << j << "] = inf";
            } else {
              //cout << "Sol [" << i << "][" << j << "] = " << distancesSol[i][j];
            }
            if (distancesFW[i][j] == numeric_limits<weight_t>::max()) {
              //cout << ", Stud[" << i << "][" << j << "] = inf";
            } else {
              //cout << ", Stud[" << i << "][" << j << "] = " << distancesFW[i][j];
            }
	      //cout << endl;
            diff++;
          }
        }
      }
    }
  }
  if(diff) { FWCorrect = false; }

  if (FWCorrect) {
    cout << "APSP using Floyd Warshall correct" << endl << endl;
  } else {
    cout << "APSP using Floyd Warshall INCORRECT" << endl << endl;
  }

  return 0;
}

static void printUsage() {
  std::cout << "--------- All Pairs Shortest Paths -help -------------------" 
            << std::endl;
  std::cout << "Usage: ./Apsp -f filename -pRes 0" << std::endl 
    << "        --OR--" << std::endl 
    << "       ./Apsp -size 10 -sparsity 0.10 -pRes 0" << std::endl 
    << "       where: " << std::endl 
    << " -f filename specifies that the file called filename"
    << "has the edge weights." << std::endl
    << "       First line of the file contains the length of n (nxn)\n" 
    << "       Each line after that gives a space seperated list of\n"
    << "              edge weights for a given node\n"
    << "       If an edge does not exist, type 'inf'.  Otherwise, specify an int.\n"
    << "       If an int is given for [i,j] (row i column j),\n"
    << "              then an edge exists from i to j\n"
    << " -pRes 1|0   whether to print full results (Default = "
    << printResult_f << ")\n"
    << " -pEdge 1|0  whether to print edge weights (Default = "
    << printMatrix_f << ")\n"
    << " -testMM 1|0 whether to test the matrix multiply sub-routine "
    << "(Default = " << testMM << ")\n\n"
    << "The following arguments apply ONLY to randomly generated sequences:\n"
    << "       (which will occur if no input file is specified)\n"
    << " -size x     specifies the matrix dimension to x (Default = " 
    << defaultMatrixSize << ")\n"
    << " -sparsity x between 0 and 1, how sparse our graph is (Default = "
    << sparsity << ")\n"
    << " -s x        specifies the random seed to use (Default = "
    << seed << ")\n\n";
}

int main(int argc, char* argv[]) {
  int matrixSize = defaultMatrixSize;
  char *myFile = NULL;
  if (argc > 1) {
    // they might just be asking for usage information
    if (std::string(argv[1]) == "-help") {
      printUsage();
      exit(0);
    }

    // or they might actually want to run the code!
    for (int i = 1; i < argc; i=i+2) {
      if (std::string(argv[i]) == "-f") {
        // We know the next argument *should* be the filename:
        myFile = argv[i + 1];
      } else if (std::string(argv[i]) == "-size") {
        // We know the next argument *should* be matrix size:
        matrixSize = atoi(argv[i + 1]);
      } else if (std::string(argv[i]) == "-sparsity") {
        // We know the next argument *should* be sparsity:
        sparsity = atof(argv[i + 1]);
        if (sparsity > 1.0 || sparsity < 0.00) {
          cout << "Sparsity must be between 0 and 1" << endl;
          exit(0);
        }
      } else if (std::string(argv[i]) == "-pRes") {
        // We know the next argument *should* be 1 or 0
        printResult_f = (atoi(argv[i + 1]) == 0) ? false : true;
      } else if (std::string(argv[i]) == "-pEdge") {
        // We know the next argument *should* be 1 or 0
        printMatrix_f = (atoi(argv[i + 1]) == 0) ? false : true;
      } else if (std::string(argv[i]) == "-testMM") {
        // We know the next argument *should* be 1 or 0
        testMM = (atoi(argv[i + 1]) == 0) ? false : true;
      } else if (std::string(argv[i]) == "-s") {
        // We know the next argument *should* be seed 
        seed = atoi(argv[i + 1]);
      }
    }
  }
  std::cout << "Note: for a complete explanation of command line arguments, type: ./Apsp -help" << std::endl;

  srand(seed);

  return apspMain(matrixSize, myFile);
}
