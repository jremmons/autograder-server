// -*- C++ -*-

/*
 * StudentSolution.cpp
 *
 * HW 04
 *
 * THIS is the only file we will look at when we grade your homework.
 * All other changes you made to any other files will be discarded.
 */

#include <algorithm>
#include <cstdlib>
#include <cilk/cilk.h>
#include <cilk/reducer_min.h>
#include <iostream>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>
using namespace std;


typedef int weight_t;

void matrixMultSequentialSolution(vector<vector<weight_t> > &matrixA, 
                                  vector<vector<weight_t> > &matrixB, 
                                  vector<vector<weight_t> > &result) {
  int size = matrixA.size();
  for(int i = 0; i<size; i++){
    for(int j = 0; j<size; j++){
      weight_t res = matrixA[i][j];
      for(int k = 0; k<size; k++) {
        weight_t tmp = numeric_limits<weight_t>::max();
        if( matrixA[i][k] != numeric_limits<weight_t>::max() && 
            matrixB[k][j] != numeric_limits<weight_t>::max() ) {
          tmp = matrixA[i][k] + matrixB[k][j];
        }
        res = (res < tmp) ? res : tmp;
      }
      result[i][j] = res;
    }
  }
}

bool apspSequentialSolution(vector<vector<weight_t> > &weights, 
                            vector<vector<weight_t> > &distances) {
  // We need to run this logb2(n) times.
  double need = (log(weights.size()) * M_LOG2E) + 1;
  for(int i = 0; i < need; i++) {
    matrixMultSequentialSolution(weights, weights, distances);
    weights.swap(distances);
  }
  /*vector<vector<weight_t> > intermediates = weights;
  for(int i = 0; i<weights.size(); i++){
    matrixMultSequential(intermediates, weights, distances);
    intermediates = distances;
  }*/

  // We'll have a negative weight cycle if and only if you can reach some node
  // from itself with negative cost.  Check if diagonals are all positive.
  weights.swap(distances); //make sure to undo that last swap.
  for(int i = 0; i<weights.size(); i++){
    if(distances[i][i]<0)
      return false;
  }

  return true;
}

void matrixMultParallelSolution(vector<vector<weight_t> > &matrixA, 
                                vector<vector<weight_t> > &matrixB, 
                                vector<vector<weight_t> > &result) {
  int size = matrixA.size();
  cilk_for(int i = 0; i<size; i++) {
    cilk_for(int j = 0; j<size; j++) {
      cilk::reducer_min<weight_t> minval;
      minval.set_value(matrixA[i][j]);
      for(int k = 0; k<size; k++) {
        weight_t tmp = numeric_limits<weight_t>::max();
        if( matrixA[i][k] != numeric_limits<weight_t>::max() && 
            matrixB[k][j] != numeric_limits<weight_t>::max() ) {
          tmp = matrixA[i][k] + matrixB[k][j];
        }
        minval.calc_min(tmp);
      }
      result[i][j] = minval.get_value();
    }
  }
}

// YOUR parallel apsp algorithm
// You may want to use/call the matrixMult subroutine you wrote, but you
// are not required to.
// Return value of FALSE indicates a Negative Weight Cycle 
// DO NOT MODIFY THIS SIGNATURE.  USED BY THE AUTOGRADER.
bool apspParallelSolution(vector<vector<weight_t> > & weights, vector<vector<weight_t> > & distances) {
  // We need to run this logb2(n) times.
  double need = (log(weights.size()) * M_LOG2E) + 1;
  for(int i = 0; i < (int)need; i++){
    matrixMultParallelSolution(weights, weights, distances);
    weights.swap(distances);
  }

  // We'll have a negative weight cycle if and only if you can reach some node
  // from its self with negative cost.  Check if diagonals are all positive.
  weights.swap(distances); //undo that last swap
  cilk::reducer_min<weight_t> minval;
  minval.set_value(distances[0][0]);
  cilk_for(int i = 0; i<weights.size(); i++){
    minval.calc_min(distances[i][i]);
  }

  return (minval.get_value()>=0);
}
