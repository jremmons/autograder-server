// -*- C++ -*-

/*
 * StudentSolution.cpp
 *
 * HW 01, cilk version from the original stocks.sml at CMU
 *
 * THIS is the only file we will look at when we grade your homework.
 * All other changes you made to any other files will be discarded.
 */

#include <cstdlib>
#include <cilk/cilk.h>
#include <iostream>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#define MIN(x, y) ((x < y) ? x : y)
#define MAX(x, y) ((x > y) ? x : y)

// YOUR naive, quadratic stock market algorithm
void solveStocksNaive(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
	for (int i = 0; i < stockSequence.size(); i++) {
		for (int j = i + 1; j < stockSequence.size(); j++) {
			int curJump = stockSequence[j] - stockSequence[i];
			if (MAX(curJump, result->jump) == curJump) {
				result->jump = curJump;
				result->bestBuy = i;
				result->bestSell = j;
			}
		}
	}
}

// YOUR recursive, divide and conqure stock market algorithm
void solveStocksRecurse(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
	if (seqLength == 1) {
		result->bestBuy  = beginIndex;
		result->bestSell = beginIndex;
		
		result->low      = stockSequence[beginIndex];
		result->high     = stockSequence[beginIndex];
		
		result->jump     = 0;
	} else {
		int newLenOne = seqLength / 2;
		int newLenTwo = seqLength - newLenOne;
		if (newLenOne + newLenTwo != seqLength) {
			cout << "LENGTHS DON'T MATCH!" << endl;
			cout << "  Expected: " << seqLength << endl;
			cout << "  Got: " << newLenOne << " + " << newLenTwo << endl;
		}
		
		StocksResult *partOneRes = new StocksResult();
		StocksResult *partTwoRes = new StocksResult();
		
		solveStocksRecurse(stockSequence, beginIndex, newLenOne, partOneRes);
		solveStocksRecurse(stockSequence, beginIndex + newLenOne, newLenTwo, partTwoRes);
		
		if (MIN(partOneRes->low,  partTwoRes->low) == partOneRes->low) {
			result->low      = partOneRes->low;
			result->bestBuy  = partOneRes->bestBuy;
		} else {
			result->low      = partTwoRes->low;
			result->bestBuy  = partTwoRes->bestBuy;
		}
		
		if (MAX(partOneRes->high,  partTwoRes->high) == partOneRes->high) {
			result->high     = partOneRes->high;
			result->bestSell = partOneRes->bestSell;
		} else {
			result->high     = partTwoRes->high;
			result->bestSell = partTwoRes->bestSell;
		}
				
		result->jump = MAX(MAX(partOneRes->jump, partTwoRes->jump), result->high - result->low);
	}
}

// YOUR recursive, divide and conqure stock market algorithm
void solveStocksRecPar(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
	if (seqLength == 1) {
		result->bestBuy  = beginIndex;
		result->bestSell = beginIndex;
		
		result->low      = stockSequence[beginIndex];
		result->high     = stockSequence[beginIndex];
		
		result->jump     = 0;
	} else {
		int newLenOne = seqLength / 2;
		int newLenTwo = seqLength - newLenOne;
		if (newLenOne + newLenTwo != seqLength) {
			cout << "LENGTHS DON'T MATCH!" << endl;
			cout << "  Expected: " << seqLength << endl;
			cout << "  Got: " << newLenOne << " + " << newLenTwo << endl;
		}
		
		StocksResult *partOneRes = new StocksResult();
		StocksResult *partTwoRes = new StocksResult();
		
		cilk_spawn solveStocksRecPar(stockSequence, beginIndex, newLenOne, partOneRes);
		cilk_spawn solveStocksRecPar(stockSequence, beginIndex + newLenOne, newLenTwo, partTwoRes);
		
		cilk_sync;
		
		if (MIN(partOneRes->low,  partTwoRes->low) == partOneRes->low) {
			result->low      = partOneRes->low;
			result->bestBuy  = partOneRes->bestBuy;
		} else {
			result->low      = partTwoRes->low;
			result->bestBuy  = partTwoRes->bestBuy;
		}
		
		if (MAX(partOneRes->high,  partTwoRes->high) == partOneRes->high) {
			result->high     = partOneRes->high;
			result->bestSell = partOneRes->bestSell;
		} else {
			result->high     = partTwoRes->high;
			result->bestSell = partTwoRes->bestSell;
		}
				
		result->jump = MAX(MAX(partOneRes->jump, partTwoRes->jump), result->high - result->low);
	}
}
