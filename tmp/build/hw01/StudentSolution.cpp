// -*- C++ -*-

/*
 * StudentSolution.cpp
 *
 * HW 01, cilk version from the original stocks.sml at CMU
 *
 * THIS is the only file we will look at when we grade your homework.
 * All other changes you made to any other files will be discarded.
 */

#include <cstdlib>
#include <cilk/cilk.h>
#include <iostream>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#define MIN(x, y) ((x < y) ? x : y)
#define MAX(x, y) ((x > y) ? x : y)

// YOUR naive, quadratic stock market algorithm
void solveStocksNaive(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
	int end_index = beginIndex + seqLength;
	int bestBuy = stockSequence[beginIndex];		// start with case of biggest jump = 0
	int bestSell = stockSequence[beginIndex];
	int jump = 0;
	int test_jump = 0;

	for(int i = beginIndex; i < end_index; ++i){
		for(int j = i + 1; j < end_index; ++j){
			test_jump = stockSequence[j] - stockSequence[i];
			if(test_jump > jump){
				bestBuy = stockSequence[i];
				bestSell = stockSequence[j];
				jump = test_jump;
			}
		}
	}

	result->bestBuy = bestBuy;
	result->bestSell = bestSell;
	result->jump = jump;

	return;
}

// YOUR recursive, divide and conqure stock market algorithm
void solveStocksRecurse(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
	if(seqLength == 1){
		int value = stockSequence[beginIndex];
		result->bestBuy = value;
		result->bestSell = value;
		result->jump = 0;
		result->low = value;
		result->high = value;
	}
	else {
		StocksResult *left = new StocksResult();
		StocksResult *right = new StocksResult();
		int halfLength = seqLength/2;
		solveStocksRecurse(stockSequence, beginIndex, halfLength, left);
		solveStocksRecurse(stockSequence, beginIndex + halfLength, seqLength - halfLength, right);
		
		result->low = MIN(left->low, right->low);
		result->high = MAX(left->high, right->high);
		
		int crossJump = right->high - left->low;
		if( (crossJump > left->jump) && (crossJump > right->jump) ){
			result->bestBuy = left->low;
			result->bestSell = right->high;
			result->jump = crossJump;
		}
		else if (left->jump > right->jump){
			result->copyBuySellJump(left);
		}
		else { //right jump is the biggest
			result->copyBuySellJump(right);
		}	
	}
	return;
}

// YOUR recursive, divide and conqure stock market algorithm
void solveStocksRecPar(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
	if(seqLength == 1){
		int value = stockSequence[beginIndex];
		result->bestBuy = value;
		result->bestSell = value;
		result->jump = 0;
		result->low = value;
		result->high = value;
	}
	else {
		StocksResult *left = new StocksResult();
		StocksResult *right = new StocksResult();
		int halfLength = seqLength/2;
		
		cilk_spawn solveStocksRecurse(stockSequence, beginIndex, halfLength, left);
		solveStocksRecurse(stockSequence, beginIndex + halfLength, seqLength - halfLength, right);
		cilk_sync;

		result->low = MIN(left->low, right->low);
		result->high = MAX(left->high, right->high);
		
		int crossJump = right->high - left->low;
		if( (crossJump > left->jump) && (crossJump > right->jump) ){
			result->bestBuy = left->low;
			result->bestSell = right->high;
			result->jump = crossJump;
		}
		else if (left->jump > right->jump){
			result->copyBuySellJump(left);
		}
		else { //right jump is the biggest
			result->copyBuySellJump(right);
		}	
	}
	return;
}
