#include <cstdlib>
#include <cilk/cilk.h>
#include <iostream>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

#define MIN(x, y) ((x < y) ? x : y)
#define MAX(x, y) ((x > y) ? x : y)

void solveStocksNaiveSolution(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
	  result->jump = 0;

	  for (int i = 0; i < seqLength; i++)
	    for (int j = i+1; j < seqLength; j++) {
	      int curr_jump = (stockSequence[j] - stockSequence[i]);
	      if (curr_jump >= result->jump) {
			result->bestBuy = stockSequence[i];
			result->bestSell = stockSequence[j];
			result->jump = curr_jump;
	      }
	    }
}

void solveStocksRecurseSolution(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
	if (seqLength == 1) {
	  result->bestBuy = result->bestSell = result->high = result->low = stockSequence[beginIndex];
	  result->jump = 0;
	} else {
	  StocksResult left_result, right_result;
	  solveStocksRecurseSolution(stockSequence, beginIndex, seqLength/2, &left_result);
	  solveStocksRecurseSolution(stockSequence, beginIndex + seqLength/2, seqLength - seqLength/2, &right_result);

	  result->low = MIN(left_result.low, right_result.low);
	  result->high = MAX(left_result.high, right_result.high);
	  if (left_result.jump > right_result.jump) {
	    result->bestBuy = left_result.bestBuy;
	    result->bestSell = left_result.bestSell;
	    result->jump = left_result.jump;
	  } else {
	    result->bestBuy = right_result.bestBuy;
	    result->bestSell = right_result.bestSell;
	    result->jump = right_result.jump;
	  }
	  if ((right_result.high - left_result.low) > result->jump) {
	    result->bestBuy = left_result.low;
	    result->bestSell = right_result.high;
	    result->jump = right_result.high - left_result.low;
	  }
	}
}

void solveStocksRecParSolution(const vector<int> &stockSequence, int beginIndex, int seqLength, StocksResult *result)
{
  if (seqLength == 1) {
    result->bestBuy = result->bestSell = result->high = result->low = stockSequence[beginIndex];
    result->jump = 0;
  } else {
    StocksResult left_result, right_result;
    
    cilk_spawn solveStocksRecParSolution(stockSequence, beginIndex, seqLength/2, &left_result);
    cilk_spawn solveStocksRecParSolution(stockSequence, beginIndex + seqLength/2, seqLength - seqLength/2, &right_result);
    cilk_sync;

    result->low = MIN(left_result.low, right_result.low);
    result->high = MAX(left_result.high, right_result.high);
    if (left_result.jump > right_result.jump) {
      result->bestBuy = left_result.bestBuy;
      result->bestSell = left_result.bestSell;
      result->jump = left_result.jump;
    } else {
      result->bestBuy = right_result.bestBuy;
      result->bestSell = right_result.bestSell;
      result->jump = right_result.jump;
    }
    if ((right_result.high - left_result.low) > result->jump) {
      result->bestBuy = left_result.low;
      result->bestSell = right_result.high;
      result->jump = right_result.high - left_result.low;
    }
  }
}

