// -*- C++ -*-

/*
 * StudentSolution.cpp
 *
 * HW 02, two ways of solving ClosestPair 
 *
 * THIS is the only file we will look at when we grade your homework.
 * All other changes you made to any other files will be discarded.
 */

#include <cstdlib>
#include <cilk/cilk.h>
#include <cilk/reducer_min.h>
#include <iostream>
#include <iostream>
#include <fstream>
#include <limits>
#include <math.h>

using namespace std;

#define MIN(a,b) ((a<b) ? a : b)
#define MAX(a,b) ((a>b) ? a : b)

void sumScanSerial(int *array, int length);
void sumScanPar(int *array, int length);
int min_reduce(PairResult** array, int start, int end);

/* 
 * This method implements the brute force method of finding closest points.
 * Use this to test the correctness of the recursive O(nlogn) method on small sets.
 * The points don't need to be sorted
 */
void solveClosestPairBrute(PointArray *points, PairResult *result) {
	int min_i = 0;
	int min_j = 1;
	int array_size = points->size();
	double min_dist = (points->getPoint(0))->distance(points->getPoint(1));	
	double dist = 0;

	for(int i = 0; i < array_size; ++i){
		for(int j = i + 1; j < array_size; ++j){
			dist = (points->getPoint(i))->distance(points->getPoint(j));
			if(dist < min_dist){
				min_i = i;
				min_j = j;
				min_dist = dist;
			}
		}
	}
	result->pointOne = points->getPoint(min_i);
	result->pointTwo = points->getPoint(min_j);
	result->distance = min_dist;	
	return;
}

/* 
 * This is the recursive method that you will complete to solve
 * the closest pair problem. It should:
 *   (1) divide -- split the input sequence in approximately half 
 *   (2) recurse -- recursively solve the two halves of the problem
 *   (3) combine -- find the solution to your input
 * The two arrays xPoints and yPoints are already sorted by x and y respectively.
 * The range of each array is (begin, end] (inclusive of begin, exclusive of end]
 */
void solveClosestPairRecurse(PointArray *xPoints, int beginX, int endX, PointArray *yPoints, int beginY, int endY, PairResult *result) {
	int arrayLength = endX - beginX;
	//base cases
	if(arrayLength <= 1){
		return;
	} else if(arrayLength == 2){
		Point *one = xPoints->getPoint(beginX);
		Point *two = xPoints->getPoint(beginX + 1);
		result->pointOne = one;
		result->pointTwo = two;
		result->distance = one->distance(two);
		return;
	} else {
		int midX = beginX + arrayLength/2;
		Point *midX_point = xPoints->getPoint(midX);
		int midX_val = midX_point->getX();	
	
		//create Y half-arrays
		int yLSize = midX - beginX;
		int yRSize = endX - midX;
		PointArray *yL = new PointArray(yLSize);
		PointArray *yR = new PointArray(yRSize);
		int yL_itr = 0;
		int yR_itr = 0;
		for(int i = 0; i < arrayLength; ++i){
			Point *point = yPoints->getPoint(i);
			if(Point::compareX(&point, &midX_point) < 0){
				yL->insert(yL_itr, point);
				yL_itr++;
			} else {
				yR->insert(yR_itr, point);
				yR_itr++;
			}
		}

		//recurse
		PairResult *left = new PairResult();
		PairResult *right = new PairResult();
		solveClosestPairRecurse(xPoints, beginX, midX, yL, 0, yLSize, left);
		solveClosestPairRecurse(xPoints, midX, endX, yR, 0, yRSize, right);
		bool left_best = true;
		double lr_min = left->distance;
		if(right->distance < left->distance){
			left_best = false;
			lr_min = right->distance;
		}
	
		//combine step -- check pairs accross midline
		//step 1: create index array from yPoints - 1 = in strip, 0 = not
		int yIndex [arrayLength];
		int yIndicator[arrayLength];
		int left_edge_val = (int)(ceil(midX_val - lr_min));
		int right_edge_val = (int)(floor(midX_val + lr_min));
		for(int i = 0; i < arrayLength; i++){
			int xVal = yPoints->getPoint(i)->getX();
			if(xVal >= left_edge_val && xVal <= right_edge_val){
				yIndex[i] = 1;
				yIndicator[i] = 1;
			} else {
				yIndex[i] = 0;
				yIndicator[i] = 0;
			}
		}	
		//sum scan on yIndex
		sumScanSerial(yIndex, arrayLength);
		
		//put y points in the correct place
		int strip_length = yIndex[arrayLength - 1];
		PointArray *y_strip = new PointArray(strip_length);
		for(int i = 0; i < arrayLength; ++i){
			if (yIndicator[i]){
				y_strip->insert(yIndex[i]-1, yPoints->getPoint(i));
			}
		}

		//Step 3 - iterate through array and check distances against lrmin
		bool better_found = false;
		int better_one = 0;
		int better_two = 0;
		double best_d = lr_min;
		for(int i = 0; i < (strip_length - 1); ++i){
			Point *itr = y_strip->getPoint(i);
			int y_max = (int)(floor(itr->getY() + lr_min));
			int j = i + 1;
			while((j < strip_length) && (y_strip->getPoint(j)->getY() <= y_max)){
				double new_d = itr->distance(y_strip->getPoint(j));
				if(new_d < best_d) {
					better_found = true;
					better_one = i;
					better_two = j;
					best_d = new_d; 
				}
				++j;
			}
		}

		//Step 4 - compare all solutions and return the best
		if(better_found){
			result->pointOne = y_strip->getPoint(better_one);
			result->pointTwo = y_strip->getPoint(better_two);
			result->distance = best_d;
		} else if(left_best){
			result->copy(left);
		} else {
			result->copy(right);
		}

		return;
	}
}

/* 
 * This is the parallel version of our recrusive method.  
 * It uses cilk_spawn and cilk_sync 
 * to improve the runtime on larger sets.
 */


void solveClosestPairParallel(PointArray *xPoints, int beginX, int endX, PointArray *yPoints, int beginY, int endY, PairResult *result) {
	int arrayLength = endX - beginX;
	//base cases
	if(arrayLength <= 1){
		return;
	} else if(arrayLength == 2){
		Point *one = xPoints->getPoint(beginX);
		Point *two = xPoints->getPoint(beginX + 1);
		result->pointOne = one;
		result->pointTwo = two;
		result->distance = one->distance(two);
		return;
	} else {
		int midX = beginX + arrayLength/2;
		Point *midX_point = xPoints->getPoint(midX);
		
		//create Y half-arrays
		int yLSize = midX - beginX;
		int yRSize = endX - midX;
		PointArray *yL = new PointArray(yLSize);
		PointArray *yR = new PointArray(yRSize);
		int yLIndicator [arrayLength];
		int yRIndicator [arrayLength];
		int yLIndex [arrayLength];
		int yRIndex [arrayLength];
		cilk_for(int i = 0; i < arrayLength; ++i){
			Point *point = yPoints->getPoint(i);
			if(Point::compareX(&point, &midX_point) < 0){
				yLIndicator[i] = 1;
				yLIndex[i] = 1;
				yRIndicator[i] = 0;
				yRIndex[i] = 0;
			} else {
				yRIndicator[i] = 1;
				yRIndex[i] = 1;
				yLIndicator[i] = 0;
				yLIndex[i] = 0;
			}
		}
		sumScanPar(yLIndex, arrayLength);
		sumScanPar(yRIndex, arrayLength);
		cilk_for(int i = 0; i < arrayLength; ++i){
			if(yLIndicator[i]){
				yL->insert(yLIndex[i] - 1, yPoints->getPoint(i));
			} else{
				yR->insert(yRIndex[i] - 1, yPoints->getPoint(i));
			}
		}

		//recurse
		PairResult *left = new PairResult();
		PairResult *right = new PairResult();
		cilk_spawn solveClosestPairParallel(xPoints, beginX, midX, yL, 0, yLSize, left);
		solveClosestPairParallel(xPoints, midX, endX, yR, 0, yRSize, right);
		cilk_sync;

		bool left_best = true;
		double lr_min = left->distance;
		if(right->distance < left->distance){
			left_best = false;
			lr_min = right->distance;
		}
	
		//combine step -- check pairs accross midline
		//step 1: create index array from yPoints - 1 = in strip, 0 = not
		int yIndex [arrayLength];
		int yIndicator[arrayLength];
		int left_edge_val = (int)(ceil(midX_point->getX() - lr_min));
		int right_edge_val = (int)(floor(midX_point->getX() + lr_min));
		cilk_for(int i = 0; i < arrayLength; i++){
			int xVal = yPoints->getPoint(i)->getX();
			if(xVal >= left_edge_val && xVal <= right_edge_val){
				yIndex[i] = 1;
				yIndicator[i] = 1;
			} else {
				yIndex[i] = 0;
				yIndicator[i] = 0;
			}
		}	
		//sum scan on yIndex
		sumScanPar(yIndex, arrayLength);

		//put y points in the correct place
		int strip_length = yIndex[arrayLength - 1];
		PointArray *y_strip = new PointArray(strip_length);
		cilk_for(int i = 0; i < arrayLength; ++i){
			if (yIndicator[i]){
				y_strip->insert(yIndex[i]-1, yPoints->getPoint(i));
			}
		}

		//Step 3 - iterate through array and check distances against lrmin
		PairResult *min_distances [MAX(strip_length - 2, 1)];
		int best_index = 0;
		if(strip_length > 2){
			cilk_for(int i = 0; i < (strip_length - 1); ++i){
				Point *itr = y_strip->getPoint(i);	
				int y_max = (int)(floor(itr->getY() + lr_min));
				int j = i + 1;
				PairResult *result = new PairResult();
				result->pointOne = itr;
				double best_d = lr_min;
				while((j < strip_length) && (y_strip->getPoint(j)->getY() <= y_max)){
					double new_d = itr->distance(y_strip->getPoint(j));
					if(new_d < best_d){
						result->distance = new_d;
						result->pointTwo = y_strip->getPoint(j);
						best_d = new_d;
					}
					++j;
				}
				min_distances[i] = result;
			}
			best_index = min_reduce(min_distances, 0, strip_length - 2);
		} else if (strip_length == 2){
			Point *point1 = y_strip->getPoint(0);
			Point *point2 = y_strip->getPoint(1);
			PairResult* result = new PairResult();
			result->distance = point1->distance(point2);
			result->pointOne = point1;
			result->pointTwo = point2;
			min_distances[0] = result;
		} else{
			min_distances[0] = new PairResult();
		}
		/*bool better_found = false;
		int better_one = 0;
		int better_two = 0;
		double best_d = lr_min;
		//parallelize this?		
		for(int i = 0; i < (strip_length - 1); ++i){
			Point *itr = y_strip->getPoint(i);
			int y_max = (int)(floor(itr->getY() + lr_min));
			int j = i + 1;
			while((j < strip_length) && (y_strip->getPoint(j)->getY() <= y_max)){
				double new_d = itr->distance(y_strip->getPoint(j));
				if(new_d < best_d) {
					better_found = true;
					better_one = i;
					better_two = j;
					best_d = new_d; 
				}
				++j;
			}
		}*/

		//Step 4 - compare all solutions and return the best
		if(min_distances[best_index]->distance < lr_min){
			result->copy(min_distances[best_index]);
		} else if(left_best){
			result->copy(left);
		} else {
			result->copy(right);
		}

		return;
	}
}

void sumScanSerial(int *array, int length){
	if(length <= 1){
		return;
	}
	
	int even_length = length/2;
	int half_array_size = even_length;
	int odd = length & 1;
	int *half_array;
	
	if(odd)
		half_array_size++;
	
	half_array = new int[half_array_size];

	//build array of pairwaise sums
	for(int i = 0; i < even_length; ++i){
		half_array[i] = array[i*2] + array[i*2 + 1];
	}
	if(odd)		//need last term
		half_array[half_array_size - 1] = array[length - 1];
	
	//recurse
	sumScanSerial(half_array, half_array_size);
	
	//expand to solution
	int* A = new int[length];
	for(int i = 0; i < length; ++i){
		A[i] = array[i];	
	}
	for(int i = 1; i < even_length; ++i){
		array[2*i - 1] = half_array[i-1];
		array[2*i] = half_array[i-1] + A[2*i];
	}
	
	array[0] = A[0];
	array[length - 1] = half_array[half_array_size - 1];	
	if(odd)
		array[length - 2] = half_array[half_array_size - 2];

	return;
}

void sumScanPar(int *array, int length){
	if(length <= 1){
		return;
	}
	
	int even_length = length/2;
	int half_array_size = even_length;
	int odd = length & 1;
	int *half_array;
	
	if(odd)
		half_array_size++;
	
	half_array = new int[half_array_size];

	//build array of pairwaise sums
	cilk_for(int i = 0; i < even_length; ++i){
		half_array[i] = array[i*2] + array[i*2 + 1];
	}
	if(odd)		//need last term
		half_array[half_array_size - 1] = array[length - 1];
	
	//recurse on pairwise sum array
	sumScanPar(half_array, half_array_size);
	
	//expand to solution
	int* A = new int[length];
	cilk_for(int i = 0; i < length; ++i){
		A[i] = array[i];	
	}
	cilk_for(int i = 1; i < even_length; ++i){
		array[2*i - 1] = half_array[i-1];
		array[2*i] = half_array[i-1] + A[2*i];
	}
	
	//edge cases
	array[0] = A[0];
	array[length - 1] = half_array[half_array_size - 1];	
	if(odd)
		array[length - 2] = half_array[half_array_size - 2];	
	return;
}

//looks through result array, returns index of one with min distance
int min_reduce(PairResult** array, int start, int end){
	if(start == end){
		return start;
	}
	int mid = (start + end)/2;
	int s1 = cilk_spawn min_reduce(array, start, mid);
	int s2 = min_reduce(array, mid + 1, end);
	cilk_sync;
	if(array[s1]->distance < array[s2]->distance){
		return s1;
	} else {
		return s2;
	}
}
