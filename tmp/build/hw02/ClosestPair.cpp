// -*- C++ -*-

/*
 * ClosestPair.cpp
 *
 * HW 02, cilk version from the original closestPair.sml at CMU
 *
 */

#include <cstdlib>
#include <cilk/cilk.h>
#include <iostream>
#include <iostream>
#include <fstream>
#include <limits>
#include "PointArray.cpp"
#include "PointSupport.cpp"
#include "PairResult.cpp"
#include "StudentSolution.cpp"
#include "Solution.cpp"
#include "Timer.cpp"

using namespace std;

// DO NOT MODIFY THIS FILE !!!!
// (the Auto-Grader will use the original version of this file,
// so any modifications you make here will be discarded before
// your assignment is graded)

#define MAX_FOR_QUADRATIC 10000

int defaultNumPoints = 10;
bool printPoints_f = false;
bool printResult_f = true;

int pairMain(int numPoints, char* myFile, unsigned int seed)
{
  
  PointArray *xPoints;
  bool executeQuadratic_f = true;
  
  // Initialize Point array
  if (myFile != NULL) {
    // get the points from the input file
    xPoints = getPointsFromFile(myFile);
  }
  else {
    // get random points
    xPoints = randomlyGeneratePoints(numPoints, seed);
  }
  
  executeQuadratic_f = (numPoints <= MAX_FOR_QUADRATIC);
  // don't print a really long sequence. that's madness
  printPoints_f = printPoints_f && executeQuadratic_f;
  if (printPoints_f)
    xPoints->print();
  
  Timer *bruteTimer = new Timer(new string("Brute Force (Quadratic)"));
  Timer *recTimer = new Timer(new string("Recursive (Serial)"));
  Timer *recParTimer = new Timer(new string("Recursive Parallel"));

  PairResult *recResult = new PairResult();
  PairResult *recParResult = new PairResult();

  PairResult *bruteResult = new PairResult();
  if (executeQuadratic_f) {
    bruteTimer->begin();
    solveClosestPairBrute(xPoints, bruteResult);
    bruteTimer->end();
    bruteTimer->print();
  }
  else {
  }

  // Now we need two arrays, one sorted by X and one by Y
  PointArray *yPoints = xPoints->copy();

  xPoints->sortByX();
  yPoints->sortByY();

  // Recursive Serial
  recTimer->begin();
  solveClosestPairRecurse(xPoints, 0, xPoints->size(), yPoints, 0, yPoints->size(), recResult);
  recTimer->end();
  recTimer->print();

  recParTimer->begin();
  solveClosestPairParallel(xPoints, 0, xPoints->size(), yPoints, 0, yPoints->size(), recParResult);
  recParTimer->end();
  recParTimer->print();

  PairResult *solResult = new PairResult();

  solveClosestPairRecurseSol(xPoints, 0, xPoints->size(), solResult);
  if (executeQuadratic_f) {
  if (bruteResult->distance != solResult->distance) {
    cout << "Brute force incorrect" << endl;
    cout << "Student solution: " << endl;
    bruteResult->print();
    cout << "Solution: " << endl;
    solResult->print();
  }
  else {
    cout << "Brute force correct" << endl;
  }
  }

  if (recResult->distance != solResult->distance) {
    cout << "Recursive incorrect" << endl;
    cout << "Student solution: " << endl;
    recResult->print();
    cout << "Solution:" << endl;
    solResult->print();
 }
  else {
    cout << "Recursive correct" << endl;
  }

  if (recParResult->distance != solResult->distance) {
    cout << "Parallel incorrect" << endl;
    cout << "Student solution:" << endl;
    recParResult->print();
    cout << "Solution:" << endl;
    solResult->print();
  }
  else {
    cout << "Parallel correct" << endl;
  }

  xPoints->~PointArray();
  yPoints->~PointArray();
  return 0;
}

int main(int argc, char* argv[])
{
  int numPoints = defaultNumPoints;
  char *myFile = NULL;
  unsigned int seed = 3291;

  if (argc > 1) {
    // they might just be asking for usage information
    if (std::string(argv[1]) == "-help") {
	std::cout << "--------- ClosestPair -help -------------------" << std::endl;
	std::cout << "Usage: ./ClosestPair -f filename -pPts 1" << std::endl 
		  << "        --OR--" << std::endl 
		  << "       ./ClosestPair -n 30" << std::endl 
		  << "       where: " << std::endl 
		  << " -f filename specifies a file called filename with the pairs in it" << std::endl
		  << "             First line of the file gives number of pairs" << std::endl
		  << "             Each line after that gives an x or y value" << std::endl
		  << " -pPts 1|0   whether you want the Points to be printed (Default = 0)" << std::endl
		  << " -pSol 1|0   whether you want the solution to be printed (Default = 1)" << std::endl
		  << " The following arguments apply ONLY to randomly generated stock sequences:" << std::endl
		  << "             (which will occur if no input file is specified)" << std::endl
		  << " -n x        limits number of Points to x (Default = " << defaultNumPoints << ")" << std::endl
		  << " -s x        specifies the seed for randomly generating points" << std::endl;
	exit(0);
    }
    
    // or they might actually want to run the code!
    for (int i = 1; i < argc; i=i+2) {
      if (std::string(argv[i]) == "-f") {
	// We know the next argument *should* be the filename:
        myFile = argv[i + 1];
      } else if (std::string(argv[i]) == "-n") {
	// We know the next argument *should* be number of Points:
	numPoints = atoi(argv[i + 1]);
      } else if (std::string(argv[i]) == "-pPts") {
	// We know the next argument *should* be 1 or 0
	printPoints_f = (atoi(argv[i + 1]) == 0) ? false : true;
      } else if (std::string(argv[i]) == "-pRes") {
	// We know the next argument *should* be 1 or 0
	printResult_f = (atoi(argv[i + 1]) == 0) ? false : true;
      } else if (std::string(argv[i]) == "-s") {
	// We know the next argument *should* be the number for the seed
	int tmp = atoi(argv[i + 1]);
	if(tmp < 0) seed = -tmp; else seed = tmp;
      } 
    }
  }
  std::cout << "Note: for a complete explanation of command line arguments, type: ./ClosestPair -help" << std::endl;

  return pairMain(numPoints, myFile, seed);

}
