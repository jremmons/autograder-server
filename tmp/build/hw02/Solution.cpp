// -*- C++ -*-

/*
 * StudentSolution.cpp
 *
 * HW 02, two ways of solving ClosestPair 
 *
 * THIS is the only file we will look at when we grade your homework.
 * All other changes you made to any other files will be discarded.
 */

#include <cstdlib>
#include <cilk/cilk.h>
#include <iostream>
#include <fstream>
#include <limits>

using namespace std;

/* 
 * This method implements the brute force method of finding closest points.
 * Use this to test the correctness of the recursive O(nlogn) method on small sets.
 * The points don't need to be sorted
 */
void solveClosestPairBruteSol(PointArray *points, PairResult *result) {

  for (unsigned int i = 0; i < points->size(); ++i) {
    for (unsigned int j = i + 1; j < points->size(); ++j) {
      if (result->distance > points->getPoint(i)->distance(points->getPoint(j))) {
        result->distance = points->getPoint(i)->distance(points->getPoint(j));
        result->pointOne = points->getPoint(i);
        result->pointTwo = points->getPoint(j);
      }
    }
  }
}

/* 
 * This is the recursive method that you will complete to solve
 * the closest pair problem. It should:
 *   (1) divide that splits the input sequence in approximately half 
 *   (2) recurse that recursively solves the two halves of the problem
 *   (3) combine that finds the solution to your input
 * The range of the array goes from (begin, end] (inclusive of begin, exclusive of end)
 */
void solveClosestPairRecurseSol(PointArray *points, int begin, int end, PairResult *result) {

  int size = end - begin;
  // Return if only one or two points
  if (size == 1 || size == 0) {
    result->distance = numeric_limits<double>::infinity();
  } 
  // If we have two points to compare!
  else if (size == 2) {
    result->distance = points->getPoint(begin)->distance(points->getPoint(begin + 1));
    result->pointOne = points->getPoint(begin);
    result->pointTwo = points->getPoint(begin + 1);
  } 
  else {
    // Get the minimum for left and right solutions
    PairResult *leftResult = new PairResult(), *rightResult = new PairResult();
    solveClosestPairRecurseSol(points, begin, begin + size/2, leftResult);
    solveClosestPairRecurseSol(points, begin + size/2, end, rightResult);
    if (leftResult->distance < rightResult->distance) {
      result->copy(leftResult);
    }
    else {
      result->copy(rightResult);
    }

    // Now we have to check the points that could potentially have a smaller spanning distance
    int farthestLeftX = begin + size/2 - 1;
    int farthestRightX = begin + size/2;
    Point * leftPoint = points->getPoint(farthestLeftX);
    Point * rightPoint = points->getPoint(farthestRightX);

    // Iterate until we find the index of the points that are too far from the center line to matter
    while(farthestLeftX > begin && (points->getPoint(farthestLeftX)->getX() - rightPoint->getX()) < result->distance) {
      farthestLeftX--;
    }
    while(farthestRightX < end - 1 && (points->getPoint(farthestRightX)->getX() - leftPoint->getX()) < result->distance) {
      farthestRightX++;
    }

    // Iterate through all the possible closest left points, comparing each to all of the possible right points.
    // Update the min_distance if we find a closer pair of points
    for (unsigned int left = farthestLeftX; left < begin + size/2; ++left) {
      for(unsigned int right = begin + size/2; right <= farthestRightX; ++right) {
        if (result->distance > points->getPoint(left)->distance(points->getPoint(right))) {
          result->distance = points->getPoint(left)->distance(points->getPoint(right));
          result->pointOne = points->getPoint(left);
          result->pointTwo = points->getPoint(right);
        }
      }
    }
    delete rightResult;
    delete leftResult;
  }

}

/* 
 * This is the parallel version of our recrusive method.  It uses cilk_spawn and cilk_sync 
 * to improve the runtime on larger sets.
 */


void solveClosestPairParallelSol(PointArray *points, int begin, int end, PairResult *result) {
  int size = end - begin;
  // Return if only one or two points
  if (size == 1 || size == 0) {
    result->distance = numeric_limits<double>::infinity();
  } 
  // If we have two points to compare!
  else if (size == 2) {
    result->distance = points->getPoint(begin)->distance(points->getPoint(begin + 1));
    result->pointOne = points->getPoint(begin);
    result->pointTwo = points->getPoint(begin + 1);
  } 
  else {
    // Get the minimum for left and right solutions
    PairResult *leftResult = new PairResult(), *rightResult = new PairResult();
    cilk_spawn solveClosestPairRecurseSol(points, begin, begin + size/2, leftResult);
    cilk_spawn solveClosestPairRecurseSol(points, begin + size/2, end, rightResult);
    cilk_sync;
    if (leftResult->distance < rightResult->distance) {
      result->copy(leftResult);
    }
    else {
      result->copy(rightResult);
    }

    // Now we have to check the points that could potentially have a smaller spanning distance
    int farthestLeftX = begin + size/2 - 1;
    int farthestRightX = begin + size/2;
    Point *leftPoint = points->getPoint(farthestLeftX);
    Point *rightPoint = points->getPoint(farthestRightX);

    // Iterate until we find the index of the points that are too far from the center line to matter
    while(farthestLeftX > begin && (points->getPoint(farthestLeftX)->getX() - rightPoint->getX()) < result->distance) {
      farthestLeftX--;
    }
    while(farthestRightX < end - 1 && (points->getPoint(farthestRightX)->getX() - leftPoint->getX()) < result->distance) {
      farthestRightX++;
    }

    // Iterate through all the possible closest left points, comparing each to all of the possible right points.
    // Update the min_distance if we find a closer pair of points
    for (unsigned int left = farthestLeftX; left < begin + size/2; ++left) {
      for(unsigned int right = begin + size/2; right <= farthestRightX; ++right) {
        if (result->distance > points->getPoint(left)->distance(points->getPoint(right))) {
          result->distance = points->getPoint(left)->distance(points->getPoint(right));
          result->pointOne = points->getPoint(left);
          result->pointTwo = points->getPoint(right);
        }
      }
    }
    delete rightResult;
    delete leftResult;
  }
}

