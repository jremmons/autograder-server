#!/bin/bash

# check if any of the environment variables are NOT set
if [ -z $WUSTLKEY ] || [ -z $CURRENTHW ] || [ -z $TESTDIR ] 
then
    echo $(date) ": environment variable(s) not set" \
	         "WUSTLKEY:" "'"$WUSTLKEY"'" "CURRENTHW:" "'"$CURRENTHW"'"   
                 "TESTDIR:"  "'"$TESTDIR"'" >> log/build_and_test.log
    echo "Environment variable(s) not set. Check that these are correct in env.sh (and WUSTLKEY in grade.sh)"
    echo "JENKINSPATH:" "'"$JENKINSPATH"'"
    echo "WUSTLKEY:" "'"$WUSTLKEY"'"
    echo "CURRENTHW:" "'"$CURRENTHW"'"
    echo "TESTDIR:" "'"$TESTDIR"'"
    exit 1
fi

# print header and the student's information
echo "WUSTL KEY:" $WUSTLKEY
echo "BUILD ID:" $BUILD_ID
echo "BUILD NUMBER:" $BUILD_NUMBER

# clean the dir for good measure (in case not cleaned on a previous run)
echo
echo "**********************************************************************"
echo "CLEAN UP THE TESTING DIRECTORY (FOR GOOD MEASURE)"
echo "**********************************************************************"
make -C $TESTDIR/$CURRENTHW clean


# copy the student's solution to the testing directory AND build the project
echo
echo "**********************************************************************"
echo "COPY STUDENT SOLUTION AND BUILD THE PROJECT"
echo "**********************************************************************"
cp -v $WORKSPACE/$CURRENTHW/StudentSolution.cpp $TESTDIR/$CURRENTHW
make -C $TESTDIR/$CURRENTHW

# test the project (test code/script should be here!)
echo
echo "**********************************************************************"
echo "TEST THE PROJECT"
echo "**********************************************************************"

echo "--TEST 1--"
echo ./Apsp -f input1
echo
$TESTDIR/$CURRENTHW/Apsp -f $TESTDIR/$CURRENTHW/input1

echo
echo "--TEST 2--"
echo ./Apsp -f input2
echo
$TESTDIR/$CURRENTHW/Apsp -f $TESTDIR/$CURRENTHW/input2

echo
echo "--TEST 3--"
echo ./Apsp -f input3
echo
$TESTDIR/$CURRENTHW/Apsp -f $TESTDIR/$CURRENTHW/input3

echo
echo "--TEST 4--"
echo ./Apsp -size 40
echo
$TESTDIR/$CURRENTHW/Apsp -size 40

echo
echo "--TEST 5--"
echo ./Apsp -s 99 -size 100
echo
$TESTDIR/$CURRENTHW/Apsp -s 9 -size 100

echo
echo "--TEST 6--"
echo ./Apsp -s 3 -size 250 -sparsity 0.7
echo
$TESTDIR/$CURRENTHW/Apsp -s 3 -size 250 -sparsity 0.7

echo
echo "--TEST 7--"
echo ./Apsp -s 77 -size 500 -sparsity 0.3
echo
$TESTDIR/$CURRENTHW/Apsp -s 77 -size 500 -sparsity 0.3


# clean up the project
echo
echo "**********************************************************************"
echo "CLEAN UP THE TESTING DIRECTORY"
echo "**********************************************************************"
make -C $TESTDIR/$CURRENTHW clean

exit 0