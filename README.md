Autograder-server
=================

##Authors:
John Emmons : john.emmons@wustl.edu : johnemmons.com

Garret Bourg : bourg.garrett@gmail.com

##Description:

This repository contains the autograding scripts and files for Fall 2014 CSE 341/549 at Washington University in St. Louis. Nonetheless, with a properly configuered server, this code can be used to autograde virtually any code-based assignments.

##Code Stucture:
**grade.sh**

The first script to be called by the autograder. When a student commits his or her changes, the Jenkins user will call this script and pass the WUSTL ID of the student as an argument. This script will then call the following scripts (in order):

1. env.sh (source environment variables)
2. build_and_test.sh
3. get_email_addr.py

Before terminating, the script will send an email to the student with the results attached.

**env.sh**

Exports all necessary environment variables (e.g. compilers, hw number, email list) so they may used in other scripts called by grade.sh 

**build_and_test.sh**

Copies, builds, and test the hw assignment submitted by the student. **All outputs** to either *STDOUT* or *STDERR* in this script are captured in a file by grade.sh and are emailed to the student upon completion. 

**get_email_addr.py**

Takes a WUSTL ID and finds the corresponding email address in lists/$STUEMAILLIST (defined in env.sh), returning the result to STDOUT.

##Configuration:

Forthcoming

**Configuring env.sh**

**Directory Permissions**

##Usage:

Forthcoming