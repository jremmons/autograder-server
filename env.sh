#!/bin/bash

# current hw assignment that is being graded dir
export CURRENTHW=hw04

# max time to spend on one students hw (seconds)
export MAXRUNTIME=600

# dir where testing HW will happen (this dir should contain $CURRENTHW)
export TESTDIR=tmp/build

# name of the file in 'lists' that contains student emails
export STUEMAILLIST=email-all

# cilk compilers, libraries, and tools environment variables
export PATH=/opt/cilkplus-install/bin:$PATH
export LD_LIBRARY_PATH=/opt/cilkplus-install/lib:/opt/cilkplus-install/lib32:/opt/cilkplus-install/lib64
export LIBRARY_PATH=/opt/cilkplus-install/lib:/opt/cilkplus-install/lib32:/opt/cilkplus-install/lib64
export PATH=/opt/cilkplus-install/cilktools/bin:$PATH