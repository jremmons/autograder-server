#!/usr/bin/env python

import sys
import datetime

"""
check if the number of command line inputs is correct (should be 2)
"""
if( len(sys.argv) != 3):
    log_err = open("log/get_email_addr.log", "a")
    # writes message to get_email_addr.log with date, time, and error
    log_err.write(datetime.date.today().strftime("%A %B %d") + " " + \
                        datetime.datetime.now().strftime("%H:%M:%S") + \
                        ": incorrect number of commandline inputs. USAGE: ./get_email_addr.py <WUSTL KEY> <EMAIL LIST PATH>\n")
    print "USAGE: ./get_email_addr.py <WUSTL ID> <EMAIL LIST PATH>"
    exit(1)

"""
set the constants from the command line inputs
"""
WUSTL_KEY = sys.argv[1]
EMAIL_LIST_PATH = sys.argv[2]

"""
open the student email file and parse it into a dictionary
"""
email_dict = { }
with open(EMAIL_LIST_PATH, "r") as email_list_file:
    for line in email_list_file.read().strip().split('\n'):
        key, value = line.split() # splits tabs or spaces
        email_dict[key] = value

"""
find the student's email and print his or her email addr to stdout 
for the autograder to pick up
"""
student_email = email_dict[WUSTL_KEY]
sys.stdout.write(student_email)
